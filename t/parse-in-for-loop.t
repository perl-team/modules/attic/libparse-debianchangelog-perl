#! /usr/bin/perl

use strict;
use warnings;

use Test::More 0.88;
use Test::Exception;

use Parse::DebianChangelog;

my $data = <<'EOT';
package (1.2-1) unstable; urgency=low

  * Initial release.

 -- Ex A. Mple <ex@example.com>  Tue, 08 Jun 2010 01:50:16 +0900
EOT

{
  my $parser = Parse::DebianChangelog->init;
  for my $x ("") {
    lives_ok { $parser->parse({ instring => $data }); } 'parse in for my $x (...) works';
  }
}

{
  my $parser = Parse::DebianChangelog->init;
  for ("") {
    lives_ok { $parser->parse({ instring => $data }); } 'parse in for (...) works';
  }
}

done_testing;
