# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Frank Lichtenheld
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: frank@lichtenheld.de\n"
"POT-Creation-Date: 2005-10-13 02:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../bin/parsechangelog:146
#, perl-format
msgid "changelog format %s not supported"
msgstr ""

#: ../bin/parsechangelog:151
msgid "ignored option -L"
msgstr ""

#: ../bin/parsechangelog:159
#, perl-format
msgid "output format %s not supported"
msgstr ""

#: ../bin/parsechangelog:168
msgid "Copyright (C) 2005 by Frank Lichtenheld\n"
msgstr ""

#: ../bin/parsechangelog:169
msgid ""
"This is free software; see the GNU General Public Licence version 2 or later "
"for copying conditions. There is NO warranty."
msgstr ""

#: ../bin/parsechangelog:200
msgid "too many arguments"
msgstr ""

#: ../bin/parsechangelog:204
#, perl-format
msgid "more than one file specified (%s and %s)"
msgstr ""

#: ../bin/parsechangelog:216 ../bin/parsechangelog:220
#, perl-format
msgid "fatal error occured while parsing %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:219
#, perl-format
msgid ""
"WARN: %s(l%s): %s\n"
"LINE: %s\n"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:221
#, perl-format
msgid "WARN: %s(l%s): %s\n"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:232
#, perl-format
msgid "FATAL: %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:279
#, perl-format
msgid "can't open file %s: %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:284
#, perl-format
msgid "can't lock file %s: %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:291
#, perl-format
msgid "can't load IO::String: %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:298
msgid "no changelog file specified"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:319
#, perl-format
msgid "found start of entry where expected %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:343
#, perl-format
msgid "bad key-value after `;': `%s'"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:347
#, perl-format
msgid "repeated key-value %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:351
msgid "badly formatted urgency value"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:362
#, perl-format
msgid "unknown key-value key %s - copying to XS-%s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:393
msgid "badly formatted heading line"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:397
#, perl-format
msgid "found trailer where expected %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:401 ../lib/Parse/DebianChangelog.pm:416
msgid "badly formatted trailer line"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:410
#, perl-format
msgid "couldn't parse date %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:425 ../lib/Parse/DebianChangelog.pm:440
#, perl-format
msgid "found change data where expected %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:458
#, perl-format
msgid "found blank line where expected %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:462 ../lib/Parse/DebianChangelog.pm:477
msgid "unrecognised line"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:486
#, perl-format
msgid "found eof where expected %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:497
#, perl-format
msgid "can't close file %s: %s"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:541
msgid "you can't combine 'count' or 'offset' with any other range option"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:545
msgid "you can only specify one of 'from' and 'since'"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:549
msgid "you can only specify one of 'to' and 'until'"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:553
msgid "'since' option specifies most recent version"
msgstr ""

#: ../lib/Parse/DebianChangelog.pm:557
msgid "'until' option specifies oldest version"
msgstr ""

#: ../lib/Parse/DebianChangelog/Util.pm:100
#, perl-format
msgid "field %s has newline then non whitespace >%s<"
msgstr ""

#: ../lib/Parse/DebianChangelog/Util.pm:102
#, perl-format
msgid "field %s has blank lines >%s<"
msgstr ""

#: ../lib/Parse/DebianChangelog/Util.pm:104
#, perl-format
msgid "field %s has trailing newline >%s<"
msgstr ""
